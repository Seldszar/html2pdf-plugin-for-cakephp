<?php
/**
 * Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * Copyright 2013, Alexandre Breteau (http://seldszar.fr)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Pdf', 'Lib');

class PdfRegistry {

/**
 * Return a new PDF instance
 *
 * @param array $settings Settings used by PDF class
 * @return Pdf A new PDF instance
 */
	public static class init($settings = array()) {
		return new Pdf($settings);
	}

}
